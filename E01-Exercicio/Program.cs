﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E01_Exercicio
{
    class Program
    {
        static void Main(string[] args)
        {
            ContaCorrente conta = new ContaCorrente();
            conta.saldo = 200;
            Console.WriteLine(conta.saldo);

            conta.saldo += 100;
            Console.WriteLine(conta.saldo);

            ContaCorrente segundaConta = new ContaCorrente();
            segundaConta.saldo = 50;

            Console.WriteLine("Primeira Conta Corrente " + conta.saldo);
            Console.WriteLine("Segunda Conta Corrente " + segundaConta.saldo);


            Console.ReadLine();
        }
    }
}

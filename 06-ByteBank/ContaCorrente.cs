﻿namespace _06_ByteBank

{
    public class ContaCorrente
    {
        public Cliente Titular {get; set;}
        public int Agencia { get; set; }
        public int Numero { get; set; }

        public int agencia;
        public int numero;
        private double _saldo = 100;

        public double Saldo
        {
            get
            {
                return _saldo;
            }
            set
            {
                if (value < 0)
                {
                    return;
                }
                _saldo = value;
            }
        }

        // Primeira Função Criada //

        public bool Sacar(double valor)
        {
            //Verificar se o saldo maior que o valor saque
            if (_saldo < valor) //Se o saldo for menor que o valor que o usuario digitou for menor e falso
            {
                return false;
            }
            _saldo -= valor; //Se o saldo por maior que o valor a ser sacado ok pode sacar
            return true;
        }
        public void Depositar(double valor)  //Trata-se de um metodo
        {
            _saldo += valor;
        }
        public bool Transferir(double valor, ContaCorrente contaDestino) //repassar valor,ContaCorrente
        {
            if (_saldo < valor)
            {
                return false;
            }
            _saldo -= valor;
            contaDestino.Depositar(valor);
            return true;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_ByteBank
{
    class Program
    {
        static void Main(string[] args)
        {
            ContaCorrente contaDaGabrilea = new ContaCorrente();
            contaDaGabrilea.titular = "Gabriela";
            contaDaGabrilea.agencia = 863;
            contaDaGabrilea.numero = 863452;

            ContaCorrente contaDaGabrielaCosta = new ContaCorrente();
            contaDaGabrielaCosta.titular = "Gabriela";
            contaDaGabrielaCosta.agencia = 863;
            contaDaGabrielaCosta.numero = 863452;

            Console.WriteLine("Igualdade de tipo de Referencia " + (contaDaGabrilea == contaDaGabrielaCosta));

            int idade = 27;
            int idadeMaisUmaVez = 27;

            Console.WriteLine("Igualdade de Tipo de Valor " + (idade == idadeMaisUmaVez));

            contaDaGabrilea = contaDaGabrielaCosta;

            Console.WriteLine(contaDaGabrilea == contaDaGabrielaCosta);

            contaDaGabrilea.saldo = 300;
            Console.WriteLine(contaDaGabrilea.saldo);
            Console.WriteLine(contaDaGabrielaCosta.saldo);

            if (contaDaGabrilea.saldo >=100)
            {
                contaDaGabrilea.saldo -= 100;
            }
            


            Console.ReadLine();
        }
    }
}

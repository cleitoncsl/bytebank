﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_ByteBank
{
    class Program
    {
        static void Main(string[] args)
        {

            ContaCorrente contaDoBruno = new ContaCorrente();
            contaDoBruno.titular = "Bruno";
            Console.WriteLine("Nome do Titutlar: " + contaDoBruno.titular);
            Console.WriteLine("Saldo Conta Corrente: " + contaDoBruno.saldo);
            //-------------->
            bool resultadoSaque = contaDoBruno.Sacar(500);
            Console.WriteLine("Saque Efetuado? " + resultadoSaque);
            Console.WriteLine("Saldo Anterior: " + contaDoBruno.saldo);
            contaDoBruno.Depositar(500);
            //-------------->
            Console.WriteLine("Saldo Conta Corrente Bruno: " + contaDoBruno.saldo);
            //-------------->
            ContaCorrente contaDaGabriela = new ContaCorrente();
            contaDaGabriela.titular = "Gabriela";
            //-------------->
            Console.WriteLine("Saldo Conta Corrente Gabriela: " + contaDaGabriela.saldo);
            //-------------->
            bool resultadoTransferencia = (contaDoBruno.Transferir(200, contaDaGabriela));
            //-------------->
            Console.WriteLine("Saldo Conta Corrente Bruno: " + contaDoBruno.saldo);
            Console.WriteLine("Saldo Conta Corrente Gabriela: " + contaDaGabriela.saldo);
            //-------------->
            contaDaGabriela.Transferir(100, contaDoBruno);
            //-------------->
            Console.WriteLine("Saldo Conta Corrente Bruno: " + contaDoBruno.saldo);
            Console.WriteLine("Saldo Conta Corrente Gabriela: " + contaDaGabriela.saldo);
            Console.ReadLine();


        }
    }
}

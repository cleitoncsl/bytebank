﻿public class ContaCorrente
{
    public string titular;
    public int agencia;
    public int numero;
    public double saldo = 100;

// Primeira Função Criada //

    public bool Sacar(double valor)
    {
        //Verificar se o saldo maior que o valor saque
        if(this.saldo < valor) //Se o saldo for menor que o valor que o usuario digitou for menor e falso
        {
            return false;
        }
        this.saldo -= valor; //Se o saldo por maior que o valor a ser sacado ok pode sacar
        return true;
    }
    public void Depositar(double valor)  //Trata-se de um metodo
    {
        this.saldo += valor;
    }
    public bool Transferir(double valor, ContaCorrente contaDestino) //repassar valor,ContaCorrente
    {
        if (this.saldo < valor)
        {
            return false;
        }

        this.saldo -= valor;
        contaDestino.Depositar(valor);
        return true;
    }
}

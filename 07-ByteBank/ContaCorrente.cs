﻿namespace _07_ByteBank

{
    public class ContaCorrente
    {
        public Cliente Titular {get; set;}

        public static int TotaldeContasCriadas { get; private set; }



        private int _agencia;

        public int Agencia
        {
            get
            {
                return _agencia;
            }
            set
            {
                if (value <= 0)
                {
                    return;
                }

                _agencia = value;
            }
        }
        public int Numero { get; set; }
        private double _saldo = 100;

        public double Saldo
        {
            get
            {
                return _saldo;
            }
            set
            {
                if (value < 0)
                {
                    return;
                }
                _saldo = value;
            }
        }

        // Primeira Função Criada //
        public ContaCorrente(int agencia, int numero)
        {
            Agencia = agencia;
            Numero = numero;

            TotaldeContasCriadas++;
        }

        public bool Sacar(double valor)
        {
            //Verificar se o saldo maior que o valor saque
            if (_saldo < valor) //Se o saldo for menor que o valor que o usuario digitou for menor e falso
            {
                return false;
            }
            _saldo -= valor; //Se o saldo por maior que o valor a ser sacado ok pode sacar
            return true;
        }
        public void Depositar(double valor)  //Trata-se de um metodo
        {
            _saldo += valor;
        }
        public bool Transferir(double valor, ContaCorrente contaDestino) //repassar valor,ContaCorrente
        {
            if (_saldo < valor)
            {
                return false;
            }
            _saldo -= valor;
            contaDestino.Depositar(valor);
            return true;
        }
    }
}

